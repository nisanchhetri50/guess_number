import random

guess_num = random.randint(1,10)
check_num = eval(input("Let's play the guessing game.\n Guess a no. between 1 & 10: "))
count = 3
case = True
while case and count >0:
    count -= 1
    if check_num == guess_num:
        print("You're right.\n You won the game!!")
        case = False
        continue
    elif check_num < guess_num:
        print("Please guess a little bit higher\n You have %s chance left" %count)
        check_num = eval(input())
    elif check_num > guess_num:
        print("Please guess a little bit lower \n You have %s chance left" %count)
        check_num = eval(input())
    else:
        print("Invalid input!!! Try again, you have", count, "chance left!!")
